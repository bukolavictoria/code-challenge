**Prerequisite**

* This project expects you to have some experience coding and checkin/out of GIT as well as some experience with AWS.
* You will need access to an AWS account for testing purpose.  If you don’t already have access to an AWS account, please sign up for a free tier one.   AWS has free tier access which allows you to learn/test for free for a period of time.  
* This review is to be submitted in BitBucket.  
* If you haven’t used Bitbucket before or don’t have an account that can be shared with Rival Technologies teams, please first create a new account.
* To create free Bitbucket account:  Go to https://bitbucket.org/product/ & sign up
* Once you have a Bitbucket account, please provide email address or username to DevOps Manager.  
* We will need to grant you access to our repository.


---

## DevOps Code Challenge

Thank you for your interest in Rival Technologies.  We like you to perform a few tasks/coding examples for our DevOps position.   

Please keep in mind that this exercise can take some time, and we don't expect candidates to make everything functional within the given time.  The goal isn’t asking candidate to spend the full 24 hours working on it.  Please see it being a challenge to see how much you can achieve within a fairly tight timeline and your judgement on when it is best to submit.  

There are 3 sections to this:

### Section 1 (Approx 30 minutes):

1. Please answer the questions in /section_1/questions.txt
2. Save answers in the same text file

### Section 2:

* To implement an infrastructure as code stack, ideally in Terraform.  If you don’t have Terraform experience, you can use AWS Cloudformation instead.

  1. Create a serverless component of an API end point using API Gateway and Lambda. 
  2. This endpoint takes the following:
    * Allow POST call only
    * Require an API key for authentication
    * POST body payload:
```
        {
            “first_name”:  “XXXX”,
            “last_name”:  “XXXX”,
            “code”:  “XXXX”,
            “phone_number”:  “XXX-XXX-XXXX”
        }
```
    
  3. When a valid call is received, this API endpoint will then send a SMS message to the phone number using AWS SNS, with the following message:
``` 
  Hi [first_name] [last_name],
  Thank you for your interest in Rival Technologies, your code is [code]
```
  4. Once sent, store the phone number in a DynamoDB table.  The table needs to be encypted.
  5. After a successful sent, this phone number can no longer received new SMS.  
  6. If the same phone number receives another POST call, your function will return an error. 

* Please save your terraform/AWS Cloudformation stack in this folder.  It should be deployable and working (so must include lambda code).

### Section 3:

* In this folder you find a csv file with a list of recipients first name, last name, verification code & phone number.

  1. Please create a script that can be run locally which will send a SMS message to all of these recipients using your API gateway endpoint.   
  2. Your script needs to log if each phone number was sent successfully or if it failed.

---

## Submit your work

Before challenge ending time (__within 24 hours from the start time__), please commit to the repo and create a pull request. And finally email the PR link to us. 

Have fun!