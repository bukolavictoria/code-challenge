resource "aws_lambda_function" "lambda" {
  filename      = "${var.name}.zip"
  function_name = "${var.name}_${var.handler}"
  role          = "${var.role}"
  handler       = "${var.name}.${var.handler}"
  runtime       = "${var.runtime}"
  publish       = true
}

# SNS Notification Service settings

resource "aws_sns_topic" "topic" {
  name = "mytopic"
  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false,
    "defaultThrottlePolicy": {
      "maxReceivesPerSecond": 1
    }
  }
}
EOF
}

resource "aws_sns_topic_subscription" "topic_lambda" {
  topic_arn = "${aws_sns_topic.topic.arn}"
  protocol  = "lambda"
  endpoint  = "${aws_lambda_function.lambda.arn}"
}


resource "aws_lambda_permission" "with_sns" {
    statement_id = "AllowExecutionFromSNS"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.lambda.arn}"
    principal = "sns.amazonaws.com"
    source_arn = "${aws_sns_topic.topic.arn}"

}

# SNS subscription

resource "aws_sns_topic_subscription" "SMS" {
  topic_arn = "${aws_sns_topic.topic.arn}"
  protocol  = "SMS"
  endpoint  = "+2348033678361"
}

# preferences 
resource "aws_sns_sms_preferences" "update_sms_prefs" {
  default_sender_id = "buklambda"

}


# invoke Config
resource "aws_lambda_function_event_invoke_config" "SNSinvoke" {
  function_name = aws_lambda_function.lambda.function_name

  destination_config {
    on_success {
      destination = aws_sns_topic.topic.arn
    }
  }
}
