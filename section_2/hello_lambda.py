import boto3
import hashlib
from boto3.dynamodb.conditions import Key

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('CUSTOMERS')

def post_handler(event, context):
    sns= boto3.client('sns')
    first_name = event['first_name']
    last_name = event['last_name']
    code = event['code']
    phone_number = event['phone_number']
    encrypted_phone_number = hashlib.sha256(phone_number.encode('utf-8')).hexdigest()
    
    items = len(table.query(KeyConditionExpression=Key('phone_number').eq(encrypted_phone_number))['Items'])
    
    #check if record(s) is returned
    if items != 0:
        return {'status' : 401, 'description' : 'Phone Number Already Exist and can no longer receive notification'}
    
    response = sns.publish(
        #TopicArn='arn:aws:sns:eu-west-1:538353771716:mytopic',
        PhoneNumber = phone_number,
        Message='Hi {first_name} {last_name},  Thanks for your interest in Rival Technologies, your code is {code}'.format(first_name=first_name, last_name=last_name, code=code)
    )
    table.put_item(Item={
        'phone_number' : encrypted_phone_number
    })
    return {"status": 200, "description": "Successful"}
